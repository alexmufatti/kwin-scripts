# KWin Scripts

These are my KWin scripts.

`centerwindow` makes a key-binding available to center the active window in the center of the screen.

`move-windows-to-desktops` is to move windows to another desktop AND change desktop at the same time, instead of the default behavior in KDE (at the time of writing this), where the window is only moved to another screen but the desktop stays the same. It provides key-bindings for up to 20 desktops.

It is recommended to put the desired script folder(s) into `~/.local/share/kwin/scripts/`.